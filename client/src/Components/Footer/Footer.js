import React from 'react';
import './Footer.css';
import heartIcon from '../../assets/heart.png';
import githubIcon from '../../assets/github.png';
import linkedIcon from '../../assets/linkedin.png';
import GitHubIcon from '@material-ui/icons/GitHub';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import FavoriteIcon from '@material-ui/icons/Favorite';

// Footer const returns the line that appear at the bottom of the homepage 
const Footer = () => {
  return (
    <div className='footer'>
      <p>
        Made with <img src={heartIcon} alt='heart'></img> by Aman
      </p>{' '}
      {/* For github & LinkedIn icons */}
      <div className='icons'>
        <a href='https://gitlab.com/SarcasticNastik/encall' className='githubIcon'>
          <GitHubIcon />
        </a>
        <a href='linkedin.com/in/aman-rojjha-131ab11a7/' className='linkIcon'>
          <LinkedInIcon />
        </a>
      </div>
    </div>
  );
};
export default Footer;
